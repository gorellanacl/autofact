<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSendsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sends', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('user_autos_id')->unsigned()->nullable();
            $table->bigInteger('questions_id')->unsigned()->nullable();
            $table->text('answer');
            $table->timestamps();
        });

        Schema::table('sends', function (Blueprint $table) {
            $table->foreign('user_autos_id')->references('id')->on('user_autos');
            $table->foreign('questions_id')->references('id')->on('questions');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sends');
    }
}
