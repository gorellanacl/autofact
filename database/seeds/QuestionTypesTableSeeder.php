<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class QuestionTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('question_types')->insert(["id" => 1, "name" => "TextBox", "code" => "BOX", 'created_at' => now()]);
        DB::table('question_types')->insert(["id" => 2, "name" => "Options", "code" => "OPT", 'created_at' => now()]);
        DB::table('question_types')->insert(["id" => 3, "name" => "Scale", "code" => "SCA", 'created_at' => now()]);
    }
}
