<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(RolTableSeeder::class);
        $this->call(QuestionTypesTableSeeder::class);
        $this->call(QuestionTableSeeder::class);
        $this->call(UserAutoTableSeeder::class);
    }
}
