<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class RolTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('roles')->insert(["id" => 1, "name" => "Admin", 'created_at' => now()]);
        DB::table('roles')->insert(["id" => 2, "name" => "Usuario", 'created_at' => now()]);
    }
}
