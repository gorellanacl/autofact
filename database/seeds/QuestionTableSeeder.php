<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class QuestionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('questions')->insert(["label" => "¿Qué te gustaría que agregáramos al informe?", "question_types_id" => 1, 'created_at' => now()]);
        DB::table('questions')->insert(["label" => "¿La información es correcta?", "question_types_id" => 2, 'created_at' => now()]);
        DB::table('questions')->insert(["label" => "¿Del 1 al 5, es rápido el sitio?", "question_types_id" => 3, 'created_at' => now()]);
    }
}
