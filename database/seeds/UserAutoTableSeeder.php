<?php

use Illuminate\Database\Seeder;

class UserAutoTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('user_autos')->insert(["roles_id" => 1, "email" => "admin@autofact.cl", "password" => md5("admin"), 'created_at' => now()]);
        DB::table('user_autos')->insert(["roles_id" => 2, "email" => "user1@autofact.cl", "password" => md5("user1"), 'created_at' => now()]);
        DB::table('user_autos')->insert(["roles_id" => 2, "email" => "user2@autofact.cl", "password" => md5("user2"), 'created_at' => now()]);
        DB::table('user_autos')->insert(["roles_id" => 2, "email" => "user3@autofact.cl", "password" => md5("user3"), 'created_at' => now()]);
    }
}
