$(document).ready(function() {

    // Login action
    $("#login-frm").submit(function() {

        var e = $(this);
        $.ajax({
            type: 'POST',
            url: e.attr("action"),
            dataType: 'json',
            headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
            data: {
                "email": $("#email").val(),
                "pass": $("#pwd").val()
            },

            success: function(data) {
                if (data.success) {
                    location.href = data.go; // redirect to form
                } else {
                    alert(data.message);
                }
            },
            error: function(data) {
                alert(data);
            }
        });


        return false;
    });


    // Save answer action
    $("#save-frm").submit(function() {

        var e = $(this);
        $.ajax({
            type: 'POST',
            url: e.attr("action"),
            dataType: 'json',
            headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
            data: {
                "answer-1": $("#field-1").val(),
                "answer-2": $("#field-2").val(),
                "answer-3": $("input[name='answer_opt']:checked").val()
            },

            success: function(data) {
                if (data.success) {
                    alert("¡Tus respuestas han sido enviadas satisfactoriamente!");
                } else {
                    alert(data.message);
                }
            },
            error: function(data) {
                alert(data);
            }
        });


        return false;
    });

});