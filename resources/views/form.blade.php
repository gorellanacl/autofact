<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>Autofact Test</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
        <link rel="stylesheet" href="css/app.css">
        <link rel="stylesheet" href="css/autofact.css">

    </head>
    <body>
        <div class="flex-center position-ref full-height">
            
            <div class="content">
                
                <div class="links">
                    @if ($rol == 1)
                    <a href="{{ route('show.sends') }}">Env&iacute;os realizados</a>
                    @endif
                    <a href="{{ url('/logout') }}">Salir</a>
                </div>
            
                <form id="save-frm" action="{{ route('actions.saveform') }}" method="post">
                    @csrf
                    
                    @foreach ($questions as $q)
                    <div class="form-group">
                        <label for="field-{{ $q->question_types_id }}">{{ $q->label }}</label>
                        @if ($q->question_types_id == 1)
                            <input type="text" id="field-1" />  
                        @elseif ($q->question_types_id == 2)
                            <select id="field-2">
                                <option value="SI">Sí</option>
                                <option value="NO">No</option>
                                <option value="MAS_O_MENOS">M&aacute;s o Menos</option>
                            <select>
                        @else
                            @for ($i=1; $i<=5; $i++)
                                <?php
                                    $checked = $i==1 ? 'checked' : '';
                                ?>
                                <input type="radio" id="val-{{ $i }}" name="answer_opt" value="{{ $i }}" {{ $checked }} />
                                <label for="val-{{ $i }}">{{ $i }}</label>
                            @endfor
                        @endif
                    </div>
                    @endforeach

                    <div><button type="submit" class="btn btn-primary">Guardar</button></div>
                </form>
            </div>
        </div>
    </body>
    <script src="js/app.js" charset="utf-8"></script>
    <script src="js/autofact.js" charset="utf-8"></script>
</html>
