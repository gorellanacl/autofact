<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>Autofact Test</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
        <link rel="stylesheet" href="css/app.css">
        <link rel="stylesheet" href="css/autofact.css">

    </head>
    <body>
        <div class="flex-center position-ref full-height">
            
            <div class="content">
                <form id="login-frm" action="{{ route('actions.login') }}" method="post">
                    @csrf
                    <div class="title m-b-md">
                        Ingreso al Sistema
                    </div>
                    <div class="form-group">
                        <label for="email">E-Mail:</label>
                        <input type="email" class="form-control" id="email" />
                    </div>
                    <div class="form-group">
                        <label for="pwd">Contrase&ntilde;a:</label>
                        <input type="password" class="form-control" id="pwd" />
                    </div>
                    <div><button type="submit" class="btn btn-primary">Ingresar</button></div>
                </form>
            </div>
        </div>
    </body>
    <script src="js/app.js" charset="utf-8"></script>
    <script src="js/autofact.js" charset="utf-8"></script>
</html>
