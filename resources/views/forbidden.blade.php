<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>Autofact Test</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
        <link rel="stylesheet" href="css/app.css">
        <link rel="stylesheet" href="css/autofact.css">

    </head>
    <body>
        <div class="flex-center position-ref full-height">
            
            <div class="content">
                
                    
                <div class="title m-b-md">
                    No est&aacute;s autorizado para ver este contenido
                </div>
                <div><a href="{{ url('/') }}">Volver al inicio</a></div>
                    
                
            </div>
        </div>
    </body>
    <script src="js/app.js" charset="utf-8"></script>
    <script src="js/autofact.js" charset="utf-8"></script>
</html>
