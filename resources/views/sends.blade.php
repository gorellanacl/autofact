<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>Autofact Test</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
        <link rel="stylesheet" href="css/app.css">
        <link rel="stylesheet" href="css/autofact.css">

    </head>
    <body>
        <div class="flex-center position-ref full-height">
            
            <div class="content">
                
                <div class="links">
                    <a href="{{ url('/form') }}">Volver a las preguntas</a>
                </div>
            
                <table class="table">
                    <tr>
                        <th>ID</th>
                        <th>Usuario</th>
                        <th>Pregunta</th>
                        <th>Respuesta</th>
                        <th>Respondido el</th>
                    </tr>
                    @foreach ($sends as $s)
                    <tr>
                        <th>{{ $s->id }}</th>
                        <th>{{ $s->userauto->email }}</th>
                        <th>{{ $s->question->label }}</th>
                        <th>{{ $s->answer }}</th>
                        <th>{{ $s->created_at }}</th>
                    </tr>
                    @endforeach
                </table>
                
            </div>
        </div>
    </body>
    <script src="js/app.js" charset="utf-8"></script>
    <script src="js/autofact.js" charset="utf-8"></script>
</html>
