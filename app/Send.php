<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Send extends Model
{
    
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'sends';

    protected $fillable = [
        'user_autos_id',
        'questions_id',
        'answer'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function userauto()
    {
        return $this->belongsTo("App\UserAuto");
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function question()
    {
        return $this->belongsTo("App\Question");
    }

}
