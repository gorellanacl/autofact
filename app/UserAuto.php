<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserAuto extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'user_autos';

    protected $fillable = [
        'roles_id',
        'email',
        'password'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function sends() {
        return $this->hasMany('App\Send');
    }
}
