<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use App\UserAuto;

use GuzzleHttp\Client;

class Actions extends Controller
{
    /**
     * Login action
     * It requires user and password
     * 
     * @return JSON string
     */
    public function login(Request $request) {

        $success = false;
        $go = "";
        $message = "";
        
        try {

            // Get data from ajax request
            $post = $request->all();

            // Check values
            $email = trim($post["email"]);
            $pass = trim($post["pass"]);

            if(empty($email) || empty($pass)) { $this->_e("Email and password are required"); }

            // Check if email is valid
            if (!filter_var($email, FILTER_VALIDATE_EMAIL)) { $this->_e("E-Mail is not valid"); }

            // Check if user exists or not
            $user = UserAuto::where(["email" => $email, "password" => md5($pass)])->first();
            if(empty($user)) { $this->_e("User doesn't exist in our records"); }

            // If user exists, create session with object
            Session::put("user", [
                "email" => $email,
                "rol" => $user->roles_id
            ]);

            $go = route("front.form");
            $success = true;


        } catch(\Exception $e) {
            $message = $e->getMessage();
            \Log::error("{{API}} " . __METHOD__ . " -> " . $message);
        }

        return response()->json([
            "success" => $success,
            "message" => $message,
            "go" => $go
        ]);
    }


    /**
     * Save answer using API
     */
    public function save(Request $request) {

        $success = false;
        $message = "";
        
        try {

            // Get data from ajax request
            $post = $request->all();

            // Check values
            $answer1 = trim($post["answer-1"]);
            $answer2 = trim($post["answer-2"]);
            $answer3 = trim($post["answer-3"]);

            if(empty($answer1) || empty($answer2) || empty($answer3)) { $this->_e("All fields are required"); }

            // Get user
            $session = Session::get("user");
            $user = UserAuto::where(["email" => $session["email"], "roles_id" => $session["rol"]])->first();
            
            // Call API
            $resp1 = $this->_saveAnswer($user->id, 1, $answer1);
            $resp2 = $this->_saveAnswer($user->id, 2, $answer2);
            $resp3 = $this->_saveAnswer($user->id, 3, $answer3);
            
            if(!$resp1 || !$resp2 || !$resp3) {
                $this->_e("Sorry, we could not save all answers");
            }

            $success = true;

        } catch(\Exception $e) {
            $message = $e->getMessage();
            \Log::error("{{API}} " . __METHOD__ . " -> " . $message);
        }

        return response()->json([
            "success" => $success,
            "message" => $message
        ]);
    }


    /**
     * Throw new exception
     */
    private function _e($message, $http = 200) {
        throw new \Exception($message, $http);
    }


    /**
     * Call API service to save answer
     */
    private function _saveAnswer($user_autos_id, $questions_id, $answer) {

        // Call API
        $client = new Client([
            "base_uri" => url("/")
        ]);
        
        // Call API
        $call = $client->request("POST", "api/send", [
            "json" => [
                "user_autos_id" => $user_autos_id,
                "questions_id" => $questions_id,
                "answer" => $answer]
        ]);
        
        // Response
        if($call->getStatusCode() == 200) {

            $response = $call->getBody()->getContents();
            $json = json_decode($response);
            
            return $json->success;

        }

        return false;
    }
}
