<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

use App\Question;

use GuzzleHttp\Client;

class Front extends Controller
{
    /**
     * Show form
     */
    public function form() {

        // Get user info
        $user = Session::get("user");
        if(!empty($user)) {

            $email = $user["email"];
            $rol = $user["rol"];

            // Load view
            return view("form", [
                "rol" => $rol,
                "email" => $email,
                "questions" => Question::all()
            ]);

        } else {
            // Not logged
            return view("forbidden");
        }
        
    }


    /**
     * Show all sends
     */
    public function show() {

        $sends = [];

        // Call API
        $client = new Client([
            "base_uri" => url("/")
        ]);
        
        // Call API
        $call = $client->request("GET", "api/send");
        
        // Response
        if($call->getStatusCode() == 200) {

            $response = $call->getBody()->getContents();
            $json = json_decode($response);
            
            if($json->success) {
                // Display all rows (sends)
                $sends = $json->data->sends;
            }

        }

        // Call view
        return view("sends", [
            "sends" => $sends
        ]);

    }
    
}
