<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Send;
use App\UserAuto;
use App\Question;

class AutofactApi extends Controller {
    
    /**
     * Get all sends by user
     * 
     * @param $userId User ID
     */
    public function list($userId = null) {

        $success = false;
        $error = "";
        $data = [];

        try {

            // Check if userId is set or not
            // If userId == empty, list all sends
            // Otherwise, sends related to user
            if(empty($userId)) {

                // List all sends
                $sends = Send::all();
                foreach($sends as $s) {
                    $s->userauto = UserAuto::find($s->user_autos_id);
                    $s->question = Question::find($s->questions_id);
                }
  
                $data["sends"] = $sends;

            } else {

                // Get user
                $user = UserAuto::find($userId);
                if(empty($user)) { $this->_e("User with ID $userId doesn't exist"); }

                // Sends by user
                $data["sends"] = Send::where("user", $user->id);

            }

            $success = true;

        } catch(\Exception $e) {
            $error = $e->getMessage();
            \Log::error("{{API}} " . __METHOD__ . " -> " . $error);
        }

        return response()->json([
            "success"   => $success,
            "error"     => $error,
            "data"      => (object)$data
        ]);
    }

    /**
     * Allow to save sends
     * 
     * @param $request POST data received
     * 
     */
    public function save(Request $request) {

        $success = false;
        $error = "";
        $data = [];

        try {

            // Check data
            if(empty($request)) { $this->_e("Empty request"); }

            // Check for required fields
            $required = ["user_autos_id", "questions_id", "answer"];
            $post = $request->all();

            if(!$this->_checkParams($post, $required)) { $this->_e("You need to send all required parameters"); }

            // Try to save to database
            $post = (object)$post;
            $send = new Send();
            $send->user_autos_id = $post->user_autos_id;
            $send->questions_id = $post->questions_id;
            $send->answer = $post->answer;

            $send->save();

            $success = true;
            $data = $send;

        } catch(\Exception $e) {
            $error = $e->getMessage();
            \Log::error("{{API}} " . __METHOD__ . " -> " . $error);
        }

        return response()->json([
            "success"   => $success,
            "error"     => $error,
            "data"      => $data
        ]);

    }


    /**
     * Throw new exception
     */
    private function _e($message, $http = 200) {
        throw new \Exception($message, $http);
    }


    /**
     * Check if required params are set or not
     */
    private function _checkParams($arr, $source) {
        
        foreach($source as $key) {
            if(!array_key_exists($key, $arr)) return false;
            if(empty($arr[$key])) return false;
        }

        return true;
    }


}
