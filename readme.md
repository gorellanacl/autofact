
## Autofact Testing

La aplicación ha sido construida con el framework Laravel 5.8, utilizando MySQL como gestor de base de datos.
Por el lado del frontend: respecto a los estilos (css), Bootstrap 4. Los llamados del front hacia el backend han sido desarrollados con JQuery.

## 1. Requerimientos

Es necesario contar con **composer** y **git** instalados en la máquina donde se clonará el proyecto.
Además de contar con:
- PHP 7.1
- MySQL 5.7+
- Nginx

_IMPORTANTE:_ Los siguientes ejemplos son considerando un ambiente con Ubuntu 18.04

Para instalar composer:

```
$ php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
$ php -r "if (hash_file('sha384', 'composer-setup.php') === '48e3236262b34d30969dca3c37281b3b4bbe3221bda826ac6a9a62d6444cdb0dcd0615698a5cbe587c3f0fe57a54d8f5') { echo 'Installer verified'; } else { echo 'Installer corrupt'; unlink('composer-setup.php'); } echo PHP_EOL;"
$ php composer-setup.php
$ php -r "unlink('composer-setup.php');"
```

Para instalar git:
```
$ sudo apt update
$ sudo apt install git
```

## 2. Instalación
Para poder poner en marcha la aplicación, por favor seguir los siguientes pasos:

a. Clonar el proyecto desde el repositorio:
```
git clone https://gorellanacl@bitbucket.org/gorellanacl/autofact.git
```

b. Instalar las librerías utilizadas en el proyecto. Estando en el root del proyecto ejecutar (lo que generará una carpeta _vendor_ con todos los elementos necesarios):
```
$ composer install
```

c. Crear una base de datos con el nombre **autofact**, autenticándose con las credenciales que correspondan:
```
$ mysql -u[nombre_usuario] -p[password]
mysql> CREATE DATABASE autofact;
```

d. Dirigirse al root del proyecto clonado, copiar el archivo .env.example como .env y configurar este nuevo archivo (**.env**) con los datos correctos para conectarse al motor de base datos:

Para copiarlo:
```
$ cp .env.example .env
```

Luego configurar/verificar lo siguiente:
```
DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=autofact
DB_USERNAME=[nombre_usuario]
DB_PASSWORD=[password]
```

e. Estando en el root del proyecto, ejecutar la migración para levantar toda la base de datos y sus valores iniciales:
```
$ php artisan migrate:fresh --seed
```

f. Para efectos de evitar cualquier problema y solo por esta vez, por favor otorgar permisos a la carpeta **storage** para que no exista problema con el sistema de _logging_ implementado. Estando en el root, ejecutar:
```
$ chmod -R 777 storage
```

g. Configurar DNS local para poder utilizar correctamente el aplicativo (levantarlo con el built-in de Laravel, genera error al intentar consumir la API, tal cual se menciona acá: https://stackoverflow.com/questions/6014958/why-does-file-get-contents-work-with-google-com-but-not-with-my-site/25651196#25651196)

Editar hosts de la máquina:
```
$ sudo nano /etc/hosts
```

Agregar al final del archivo:
```
127.0.0.1   autofact.local.cl
```

h. Configurar en NGinx la aplicación para su puesta en marcha

Dirigirse a los sitios disponibles de NGinx:
```
$ cd /etc/nginx/sites-available
```

Crear un archivo de configuración, utilizaremos nano para crearlo y completarlo eventualmente
```
$ sudo nano autofact
```

La configuración utilizada para desarrollarlo, es la siguiente (probablemente se deba hacer algún ajuste respecto al ambiente donde está siendo desplegado):
```
server {
	
	server_name autofact.local.cl;
        root [RUTA_HACIA_PROYECTO]/autofact/public;
        index index.php index.html;
        
	location / {
		try_files $uri $uri/ /index.php?$args;
	}

	# pass PHP scripts to FastCGI server
	#
	location ~ \.php$ {
	#
	#	# With php-fpm (or other unix sockets):
		fastcgi_pass unix:/var/run/php/php7.1-fpm.sock;
		include snippets/fastcgi-php.conf;
		fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;
	}

	# deny access to .htaccess files, if Apache's document root
	# concurs with nginx's one
	#
	location ~ /\.ht {
		deny all;
	}
}
```

Crear un symbolic link para dejarlo disponible
```
$ sudo ln -s /etc/nginx/sites-available/autofact /etc/nginx/sites-enabled/
```

Reiniciar NGinx para que tome los cambios
```
$ systemctl restart nginx
```


Si todo salió correcto, debería quedar expuesto el sistema a través de una URL del tipo:
```
http://autofact.local.cl
```

### **IMPORTANTE**: Si aparece algún error de "permission denied", por favor repetir el **punto F**

## 3. Datos de Prueba

Admin:
Email: admin@autofact.cl
Contraseña: admin

Usuarios normales:
Email: user1@autofact.cl
Contraseña: user1

Email: user2@autofact.cl
Contraseña: user2

Email: user3@autofact.cl
Contraseña: user3

## 4. API

Tipo: GET
Descripción: Lista todos los envíos, y si se pasa el _userId_, entrega los envíos solo del usuario.
Endpoint: http://autofact.local.cl/api/send/{userId}

Tipo: POST
Descripción: Graba el envío en la base de datos
Parámetros (JSON):
- _user_autos_id_: ID del usuario
- _questions_id_: ID de la pregunta
- _answer_: Respuesta de la pregunta

Endpoint: http://autofact.local.cl/api/send