<?php

use Illuminate\Support\Facades\Session;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


// Ajax actions
Route::post("/login", "Actions@login")->name("actions.login");
Route::post("/save", "Actions@save")->name("actions.saveform");

// Views
Route::get("/form", "Front@form")->name("front.form");
Route::get("/show-sends", "Front@show")->name("show.sends");
Route::get("/logout", function() {
    Session::flush();
    return redirect("/");
});
